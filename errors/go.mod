module gitee.com/chenwitt/pkg/errors

go 1.20

require (
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.21.0
)
