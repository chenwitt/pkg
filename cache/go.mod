module gitee.com/chenwitt/pkg/cache

go 1.20

require (
	gitee.com/chenwitt/pkg/compression v0.0.0-20230815043409-17b677b9c007
	gitee.com/chenwitt/pkg/errors v0.0.0-20230815043409-17b677b9c007
	gitee.com/chenwitt/pkg/timeutil v0.0.0-20230815043409-17b677b9c007
	gitee.com/chenwitt/pkg/trace v0.0.0-20230815043409-17b677b9c007
	github.com/go-redis/redis/v7 v7.4.1
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.21.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
