module gitee.com/chenwitt/pkg/sign

go 1.20

require (
	gitee.com/chenwitt/pkg/timeutil v0.0.0-20230815043409-17b677b9c007
	github.com/pkg/errors v0.9.1
)
