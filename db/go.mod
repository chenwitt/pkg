module gitee.com/chenwitt/pkg/db

go 1.20

require (
	go.uber.org/zap v1.21.0
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.2
)

require (
	gitee.com/chenwitt/pkg/errors v0.0.0-20230815043409-17b677b9c007 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
