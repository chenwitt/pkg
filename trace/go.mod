module gitee.com/chenwitt/pkg/trace

go 1.20

require go.uber.org/zap v1.21.0

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
