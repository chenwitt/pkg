module gitee.com/chenwitt/pkg/httpclient

go 1.20

require go.uber.org/zap v1.21.0

require (
	gitee.com/chenwitt/pkg/errors v0.0.0-20230815043409-17b677b9c007 // indirect
	gitee.com/chenwitt/pkg/trace v0.0.0-20230815043409-17b677b9c007 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
