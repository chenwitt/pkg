module gitee.com/chenwitt/pkg/routine

go 1.20

replace gitee.com/chenwitt/pkg/errors => ../errors

require gitee.com/chenwitt/pkg/errors v0.0.0-00010101000000-000000000000

require (
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.11.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
)
